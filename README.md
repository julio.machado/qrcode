## Instalação:
1. Conferir se o python 2 está instalado:

`python -V`

2. Instalar 'lxml' e 'qrencode':

`sudo apt install python-lxml qrencode`