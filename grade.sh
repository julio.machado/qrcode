#!/bin/bash
echo "Selecione o sub domíno do cliente"
read cliente
if [[ $cliente == *.* ]]
then
  echo "Apenas considerar a parte anterior a '.arkmeds.com'"
  exit 1
fi
echo "Selecione o número inicial"
read numero_inicial
echo "Deseja incluir numeração? (Y/n)"
read is_numerated
echo "Selecione o tamanho do QRCode. 1 para 3x2, 2 para 4.5x3 e 3 para 6x4:"
read tamanho

if [ "$tamanho" -eq 1 ]
then
	jfactor=12
	kfactor=168
	resize_height=66.06
	resize_qrcode=66
	resize_width=106.31
elif [ "$tamanho" -eq 2 ]
then
	jfactor=9
	kfactor=72
	resize_height=99.09
	resize_qrcode=99.09
	resize_width=159.465
elif [ "$tamanho" -eq 3 ]
then
	jfactor=6
	kfactor=42
	resize_height=132.12
	resize_qrcode=132.12
	resize_width=212.62
else
	echo "Valor de tamanho inválido!"
	exit 1
fi


echo "Selecione o número de QRCodes ( $kfactor QR por páginas)"
read num_qrcode
pagina=$(( ($num_qrcode + $kfactor -1)/$kfactor ))

rm temp/*
mkdir -p temp
mkdir -p clientes/$cliente/pdf
cp arkmeds.svg clientes/$cliente
cp arkmeds.svg temp/
sed -i '5c\       width="'${resize_width}'"' src/template
sed -i '6c\       height="'${resize_height}'"' src/template
./src/svg-resize.py --height "$resize_qrcode"px clientes/$cliente/arkmeds.svg

for k in $(seq 0 $((pagina-1)))
do
    echo "gerando a pagina $k"
    for j in $(seq 0 $((kfactor/jfactor -1)))
    do
        for i in $(seq 0 $((jfactor -1)))
        do
            numero=$(( $k*$kfactor+$j*$jfactor+$i+$numero_inicial ))
			identificador=$( printf "%05d\n" $(( $k*$kfactor+$j*$jfactor+$i+$numero_inicial )))
            echo "numero $numero"
			if [ "$is_numerated" != "n" ]
			then
				sed -i 's/######/'$identificador'/g' clientes/$cliente/arkmeds.svg
			fi
            qrencode -s 2 -t svg -o temp/qr_temp.svg "$cliente.arkmeds.com/qr_code/equipamento/$numero"
            ./src/svg-resize.py --width "$resize_qrcode"px temp/qr_temp.svg
			./src/svg-resize.py --width "$resize_qrcode"px temp/qr_temp.svg
			./src/svg_stack.py --direction=h clientes/$cliente/arkmeds.svg temp/qr_temp.svg>temp/temp.svg
			sed -i '$d' temp/temp.svg
			cat src/template >> temp/temp.svg
		    if [ -e temp/linha-$j.svg ]
		    then
		    	cp temp/linha-$j.svg temp/linha_temp.svg
		    	./src/svg_stack.py --direction=h --margin=7.559055 temp/linha_temp.svg temp/temp.svg>temp/linha-$j.svg
				rm temp/linha_temp.svg
			else
				cp temp/temp.svg temp/linha-$j.svg
			fi
			sed -i 's/'$identificador'/######/g' temp/arkmeds.svg
			rm temp/qr_temp.svg
		done
	    if [ -e temp/pagina_grid-$k.svg ]
	    then
	    	cp temp/pagina_grid-$k.svg temp/pagina_grid_temp.svg
	    	./src/svg_stack.py --direction=v --margin=7.559055  temp/pagina_grid_temp.svg temp/linha-$j.svg>temp/pagina_grid-$k.svg
			rm temp/pagina_grid_temp.svg
		else
			cp temp/linha-0.svg temp/pagina_grid-$k.svg
		fi
		rm temp/linha-$j.svg
	done
	sed 's/<\/svg>//g' temp/pagina_grid-$k.svg > temp/pagina-$k.svg
	rm temp/pagina_grid-$k.svg
	rm temp/temp.svg
	inkscape --file=temp/pagina-$k.svg --export-area-drawing --without-gui --export-pdf=clientes/$cliente/pdf/pagina-$k.pdf
	rm temp/pagina-$k.svg
done
zip clientes/$cliente/qrcodes.zip clientes/$cliente/pdf/*
